#pragma once
#include <vector>
using namespace std;

struct point
{
	double x;
	double y;
	double potential;
	vector <int> NearTriangels;
	vector <int> NearPoints;

	bool operator == (const point &a)
	{
		if ((x == a.x) && (y == a.y))
			return true;
		else return false;
	}
};


class Triangle
{
public:
	point A;
	point B;
	point C;

	double CircleX;
	double CircleY;

	double CircleRadius;

	Triangle();
	Triangle(point my_A, point my_B, point my_C);

};