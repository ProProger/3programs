//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется FDTD.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_FDTD_DIALOG                 102
#define IDR_MAINFRAME                   128
#define IDC_BUTTON1                     1000
#define IDC_EDIT1                       1001
#define IDC_E0                          1001
#define IDC_GRAF1                       1002
#define IDC_MU0                         1003
#define IDC_DT                          1004
#define IDC_DZ                          1005
#define IDC_DY                          1006
#define IDC_GRAF2                       1007
#define IDC_STOP                        1008
#define IDC_GRAF3                       1008
#define IDC_FDTD                        1009
#define IDC_FDTD2                       1010
#define IDC_PSTD                        1010
#define IDC_FDTD_2D                     1011
#define IDC_FDTD_2D2                    1012
#define IDC_PSTD_2D2                    1012
#define IDC_NEV                         1013
#define IDC_SMESH                       1014
#define IDC_LIN                         1015
#define IDC_SPLINE                      1016

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1017
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
