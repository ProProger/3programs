#include "StdAfx.h"
#include "Triangle.h"
#include <math.h>

Triangle::Triangle()
{
	A.x = A.y = B.x = B.y = C.x = C.y = 0;
}
Triangle::Triangle(point my_A, point my_B, point my_C)
{
	A.x = my_A.x;
	A.y = my_A.y;
	B.x = my_B.x;
	B.y = my_B.y;
	C.x = my_C.x;
	C.y = my_C.y;

	/*	double a,b,c,d,e,f;

	a=-2*(A.x-C.x);
	b=-2*(A.y-C.y);
	c=A.x*A.x+A.y*A.y-C.x*C.x-C.y*C.y;

	d=-2*(A.x-B.x);
	e=-2*(A.y-B.y);
	f=A.x*A.x+A.y*A.y-B.x*B.x-B.y*B.y;

	double delta;

	delta = a*e-b*d;

	CircleX = (-c*e+f*b)/delta;
	CircleY = (-f*a+c*d)/delta;
	CircleRadius=sqrt((A.x-CircleX)*(A.x-CircleX)+(A.y-CircleY)*(A.y-CircleY));
	*/
	/*	CircleY=(d*c-f*a)/(e*a-d*b);
	CircleX=(-c-b*CircleY)/a;
	CircleRadius=sqrt((A.x-CircleX)*(A.x-CircleX)+(A.y-CircleY)*(A.y-CircleY));
	*/
	CircleX = (C.x*C.x*(A.y - B.y) + (A.x*A.x + (A.y - B.y)*(A.y - C.y))*(B.y - C.y) + B.x*B.x*(-A.y + C.y)) /
		(2 * (C.x*(A.y - B.y) + A.x*(B.y - C.y) + B.x*(-A.y + C.y)));
	CircleY = -((2 * (A.x - C.x)*(A.x*A.x - B.x*B.x + A.y*A.y - B.y*B.y) - 2 * (A.x - B.x)*(A.x*A.x - C.x*C.x + A.y*A.y - C.y*C.y)) /
		(4 * (C.x*(A.y - B.y) + A.x*(B.y - C.y) + B.x*(-A.y + C.y))));
	CircleRadius = sqrt((A.x - CircleX)*(A.x - CircleX) + (A.y - CircleY)*(A.y - CircleY));

}
