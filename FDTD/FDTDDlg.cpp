
// FDTDDlg.cpp : ���� ����������
//

#include "stdafx.h"
#include "FDTD.h"
#include "FDTDDlg.h"
#include "afxdialogex.h"
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <fstream>
#include <iostream>
#include "fourea.h"
#include <fstream>


#ifdef _DEBUG
#define new DEBUG_NEW
#endif
//#define SETPOINT(x,y) pic.memDC->SetPixel((x),(y),RGB((ez[i][j]-minVal)/(maxVal-minVal)*255*one,(ez[i][j]-minVal)/(maxVal-minVal)*255*two,0/*(ez[i][j]-minVal)/(maxVal-minVal)*255*/))
#define SETPOINT(x,y) pic.memDC1->SetPixel((x),(y),RGB((ez[i][j][IE/2])*255*one,(ez[i][j][IE/2])*255*two,0/*(ez[i][j]-minVal)/(maxVal-minVal)*255*/))
//#define SETPOINT2(x,y) pic2.memDC2->SetPixel((x),(y),RGB((ez[IE/2][i][j])*255*one,(ez[IE/2][i][j])*255*two,0))
#define SETPOINT2(y,z) pic2.memDC2->SetPixel((y),(z),RGB((ez[IE / 2][i][j])*255*one2,(ez[IE / 2][i][j])*255*two2,0/*(ez[i][j]-minVal)/(maxVal-minVal)*255*/))
#define SETPOINT3(y,z) pic3.memDC3->SetPixel((y),(z),RGB((ez[i][19][j])*255*one2,(ez[i][19][j])*255*two2,0/*(ez[i][j]-minVal)/(maxVal-minVal)*255*/))

double pi = 3.1415926/*f*/;
// ���������� ���� CAboutDlg ������������ ��� �������� �������� � ����������

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// ������ ����������� ����
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV

// ����������
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// ���������� ���� CFDTDDlg



CFDTDDlg::CFDTDDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CFDTDDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CFDTDDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CFDTDDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_FDTD_2D, &CFDTDDlg::OnBnClickedFdtd2d)
END_MESSAGE_MAP()


// ����������� ��������� CFDTDDlg

BOOL CFDTDDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// ���������� ������ "� ���������..." � ��������� ����.

	// IDM_ABOUTBOX ������ ���� � �������� ��������� �������.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// ������ ������ ��� ����� ����������� ����.  ����� ������ ��� �������������,
	//  ���� ������� ���� ���������� �� �������� ����������
	SetIcon(m_hIcon, TRUE);			// ������� ������
	SetIcon(m_hIcon, FALSE);		// ������ ������

	// TODO: �������� �������������� �������������
	// TODO: �������� �������������� �������������
	setka_pen.CreatePen(	//����� ��� ��������� �����
		PS_DOT,
		1,
		RGB(255, 255, 0));

	osi.CreatePen(	//����� ��� ����
		PS_SOLID,		//�������� �����		
		2,				//������� 1 �������
		RGB(255, 255, 0));	//���� ������

	graphic.CreatePen(	//����� ��� ����
		PS_SOLID,		//�������� �����		
		2,				//������� 1 �������
		RGB(0, 255, 0));	//���� ������

	red_pen.CreatePen(	//����� ��� ����
		PS_SOLID,		//�������� �����		
		2,				//������� 1 �������
		RGB(255, 0, 0));	//���� �������
	white.CreatePen(	//����� ��� ����
		PS_SOLID,		//�������� �����		
		1,				//������� 1 �������
		RGB(255, 255, 255));

	/*e1.CreatePen(	//����� ��� ����
		PS_SOLID,		//�������� �����		
		2,				//������� 1 �������
		RGB(255, 0, 0));	//���� �������
	e2.CreatePen(	//����� ��� ����
		PS_SOLID,		//�������� �����		
		2,				//������� 1 �������
		RGB(0, 255, 0));	//���� �������
	e3.CreatePen(	//����� ��� ����
		PS_SOLID,		//�������� �����		
		2,				//������� 1 �������
		RGB(0, 0, 255));	//���� �������
	e4.CreatePen(	//����� ��� ����
		PS_SOLID,		//�������� �����		
		2,				//������� 1 �������
		RGB(0, 255, 255));	//���� �������
	e5.CreatePen(	//����� ��� ����
		PS_SOLID,		//�������� �����		
		2,				//������� 1 �������
		RGB(0, 0, 0));	//���� �������*/
	e1.CreateSolidBrush(RGB(255, 0, 0));
	e2.CreateSolidBrush(RGB(0, 255, 0));
	e3.CreateSolidBrush(RGB(0, 0, 255));
	e4.CreateSolidBrush(RGB(0, 255, 255));
	e5.CreateSolidBrush(RGB(0, 0, 0));

	frameWnd1 = GetDlgItem(IDC_GRAF1);
	frameDc1 = frameWnd1->GetDC();
	frameWnd1->GetClientRect(&r1);

	frameWnd2 = GetDlgItem(IDC_GRAF2);
	frameDc2 = frameWnd2->GetDC();
	frameWnd2->GetClientRect(&r2);

	frameWnd3 = GetDlgItem(IDC_GRAF3);
	frameDc3 = frameWnd3->GetDC();
	frameWnd3->GetClientRect(&r3);

	kn_1 = false;
	/*N = 20;
	ez = new double**[N];
	hx = new double**[N];
	hy = new double**[N];

	for (int i = 0; i<N; i++)
	{
		ez[i] = new double *[N];
		hx[i] = new double *[N];
		hy[i] = new double *[N];
	}
	for (int i = 0; i<N; i++)
	{
		for (int j = 0; j<N; j++)
		{
			ez[i][j] = new double[N];
			hx[i][j] = new double[N];
			hy[i][j] = new double[N];
		}
	}*/

	pic.Create1(this, IDC_GRAF1);
	pic2.Create2(this, IDC_GRAF2);
	pic3.Create3(this, IDC_GRAF3);
	hCalcTh_FDTD = NULL;


	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

void CFDTDDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// ��� ���������� ������ ����������� � ���������� ���� ����� ��������������� ����������� ���� �����,
//  ����� ���������� ������.  ��� ���������� MFC, ������������ ������ ���������� ��� �������������,
//  ��� ������������� ����������� ������� ��������.

void CFDTDDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // �������� ���������� ��� ���������

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// ������������ ������ �� ������ ����������� ��������������
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// ��������� ������
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
		CDC *MDc1 = new CDC();
		MDc1->CreateCompatibleDC(frameDc1);
		CBitmap bmp1;
		bmp1.CreateCompatibleBitmap(frameDc1, r1.Width(), r1.Height());
		CBitmap *tmp1 = MDc1->SelectObject(&bmp1);

		
		if (kn_1 == true)
		{
				//Plot(frameWnd1, IDC_GRAF1, frameDc1, r1, MDc1, bmp1, tmp1, &graphic, IE, 0, xp1, yp1, EZ, IE);
		}

		frameDc1->BitBlt(0, 0, r1.Width(), r1.Height(), MDc1, 0, 0, SRCCOPY);
		delete MDc1;

	}
}

// ������� �������� ��� ������� ��� ��������� ����������� ������� ��� �����������
//  ���������� ����.
HCURSOR CFDTDDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CFDTDDlg::Plot(CWnd* frameWnd, int picID, CDC* frameDc, CRect r, CDC *MDc, CBitmap &bmp, CBitmap *tmp, CPen *ruchka,
	double xmax, double xmin, double xpi, double ypi, double y_1[IE], int n)
{
	UpdateData(true);
	double max = 0, min = 0;
	int ymax, ymin;
	/*for (int i = 0; i < n; i++)
	{
		if (max < y_1[i]) max = y_1[i];
	}

	for (int i = 0; i < n; i++)
	{
		if (min > y_1[i]) min = y_1[i];
	}*/
	ymax = 8;
	ymin = -8;

	xpi = ((double)(r.Width()) / (xmax - xmin));   //������������ ��������� ��������� �� �
	ypi = -((double)(r.Height()) / (ymax - ymin));//Y

	MDc->FillSolidRect(&r, RGB(0, 0, 0));//���

	//MDc->SelectObject(&ruchka);
	//MDc->SelectObject(&osi);

	/*MDc->SelectObject(&setka_pen);
	if (r == r1)
	{
		//��������� ����� �� y
		for (double j = 0; j <= xmax; j += xmax / 8)
		{
			MDc->MoveTo((xpi*((j)-xmin)), (ypi*(ymax - ymax)));
			MDc->LineTo((xpi*((j)-xmin)), (ypi*(ymin - ymax)));//(KOORD(x, ymin));
		}
		//��������� ����� �� x
		for (double i = 0; i <= ymax; i += (double)(ymax/2))//+2
		{
			MDc->MoveTo((xpi*((0) - xmin)), (ypi*((i)-ymax)));
			MDc->LineTo((xpi*((xmax)-xmin)), (ypi*((i)-ymax)));
		}
		for (double i = 0; i >= ymin; i -= (double)(ymax/2))//+2
		{
			MDc->MoveTo((xpi*((0) - xmin)), (ypi*((i)-ymax)));
			MDc->LineTo((xpi*((xmax)-xmin)), (ypi*((i)-ymax)));
		}
	}*/


	MDc->SelectObject(&red_pen);
	//MDc->SelectObject(&e1);
	for (int i = 0; i < n-1; i++)
		{
			MDc->MoveTo((xpi*(i - xmin)), (ypi*(y_1[i] - ymax)));
			MDc->LineTo((xpi*(i + 1 - xmin)), (ypi*(y_1[i + 1] - ymax)));
			//MDc->MoveTo((xpi*(mass_x_ner[i] - xmin)), (ypi*(y_1[i] - ymax)));
			//MDc->LineTo((xpi*(mass_x_ner[i+1] - xmin)), (ypi*(y_1[i + 1] - ymax)));
		}
	UpdateData(false);
}




void /*FDTDCalc*/CFDTDDlg::CalcAntonFDTD(CPicControl1 & pic1, CPicControl2 & pic2, CPicControl3 & pic3)
{
	/*vector<vector<double>>	dz(IE, vector<double>(JE, 0)),
		ez(IE, vector<double>(JE, 0)),
		ga(IE, vector<double>(JE, 0));

	vector<vector<double>>	hx(IE, vector<double>(JE, 0)),
		hy(IE, vector<double>(JE, 0));*/
		
	double ex[IE][IE][IE];
	double ey[IE][IE][IE];
	double ez[IE][IE][IE];
	double hx[IE][IE][IE];
	double hy[IE][IE][IE];
	double hz[IE][IE][IE];

		int l, n, i, j, ic, jc, nsteps, npml, istart, jstart, ifin, jfin;
		double ddx, dt, T, epsz, pi, epsilon, sigma, eaf;
		double xn, xxn, xnum, xd, curl_e;
		double t0, spread, pulse;
		double gi2[IE], gi3[IE];
		double gj2[JE], gj3[IE];
		double fi1[IE], fi2[IE], fi3[JE];
		double fj1[JE], fj2[JE], fj3[JE];
		double ihx[IE][IE][IE], ihy[IE][IE][IE], ihz[IE][IE][IE];
		double tau, chi1, del_exp;


		FILE *fp, *fopen();
		ic = IE / 2;
		jc = JE / 2;
		ddx = .01; // Cell size 
		dt = ddx / 6e8; // Time steps 
		epsz = 8.8e-12;
		pi = 3.14159;
		// Initialize the arrays 
		for (j = 0; j < IE; j++)
		{
			//printf( "%2d ",j);
			for (i = 0; i < IE; i++)
			{
				for (int k = 0; k < IE; k++)
				{
					//dz[i][j][k] = 0.0;
					ex[i][j][k] = 0.0;
					ey[i][j][k] = 0.0;
					ez[i][j][k] = 0.0;

					hx[i][j][k] = 0.0;
					hy[i][j][k] = 0.0;
					hz[i][j][k] = 0.0;

					ihx[i][j][k] = 0.0;
					ihy[i][j][k] = 0.0;
					//ga[i][j][k] = 1.0;
				}
			}
		}
		// Calculate the PML parameters 
		for (i = 0; i < IE; i++)
		{
			gi2[i] = 1.0;
			gi3[i] = 1.0;
			fi1[i] = 0.0;
			fi2[i] = 1.0;
			fi3[i] = 1.0;
		}
		for (j = 0; j < JE; j++)
		{
			gj2[j] = 1.0;
			gj3[j] = 1.0;
			fj1[j] = 0.0;
			fj2[j] = 1.0;
			fj3[j] = 1.0;
		}
		//printf( "Number of PML cells --> ");
		//scanf("%d", &npml);

		npml = 4;

		for (i = 0; i <= npml; i++)
		{
			xnum = npml - i;
			xd = npml;
			xxn = xnum / xd;
			xn = 0.25*pow(xxn, 3.0);

			gi2[i] = 1.0 / (1.0 + xn);
			gi2[IE - 1 - i] = 1.0 / (1.0 + xn);
			gi3[i] = (1.0 - xn) / (1.0 + xn);
			gi3[IE - i - 1] = (1.0 - xn) / (1.0 + xn);
			xxn = (xnum - .5) / xd;
			xn = 0.25*pow(xxn, 3.0);
			fi1[i] = xn;
			fi1[IE - 2 - i] = xn;
			fi2[i] = 1.0 / (1.0 + xn);
			fi2[IE - 2 - i] = 1.0 / (1.0 + xn);
			fi3[i] = (1.0 - xn) / (1.0 + xn);
			fi3[IE - 2 - i] = (1.0 - xn) / (1.0 + xn);
		}
		for (j = 0; j <= npml; j++)
		{
			xnum = npml - j;
			xd = npml;
			xxn = xnum / xd;
			xn = 0.25*pow(xxn, 3.0);

			gj2[j] = 1.0 / (1.0 + xn);
			gj2[JE - 1 - j] = 1.0 / (1.0 + xn);
			gj3[j] = (1.0 - xn) / (1.0 + xn);
			gj3[JE - j - 1] = (1.0 - xn) / (1.0 + xn);
			xxn = (xnum - .5) / xd;
			xn = 0.25*pow(xxn, 3.0);
			fj1[j] = xn;
			fj1[JE - 2 - j] = xn;
			fj2[j] = 1.0 / (1.0 + xn);
			fj2[JE - 2 - j] = 1.0 / (1.0 + xn);
			fj3[j] = (1.0 - xn) / (1.0 + xn);
			fj3[JE - 2 - j] = (1.0 - xn) / (1.0 + xn);
		}


		istart = 0;
		jstart = 0;
		ifin = 0;
		jfin = 0;
		epsilon = 2;
		sigma = 0.01;
		chi1 = 2;
		tau = 0.001;

		del_exp = exp(-dt / tau);
		tau = 1.e-6*tau;


		t0 = 0.0;
		spread = 40.0;
		T = 0;
		nsteps = 1;
		double DT = 0.5;// / 4;
		while (nsteps > 0)
		{
			//printf("nsteps --> ");
			//scanf("%d", &nsteps);
			//printf("%d \n", nsteps);
			for (n = 1; n <= nsteps; n++)
			{
				T = T + 1;
				/* ---- Start of the Main FDTD loop ---- */
				/* Calculate the Dz field */
				for (i = 1; i < JE; i++)
				{
					for (j = 1; j < JE; j++)
					{
						for (int k = 1; k < JE; k++)
						{
							ex[i][j][k] = gi3[i] * gj3[j] * gj3[k] * ex[i][j][k]
								+ gi2[i] * gj2[j] * gj2[k] * DT*(hz[i][j][k] - hz[i][j-1][k]
								- hy[i][j][k] + hy[i][j][k-1]);


							ey[i][j][k] = gi3[i] * gj3[j] * gj3[k] * ey[i][j][k]
								+ gi2[i] * gj2[j] * gj2[k] * DT*(hx[i][j][k] - hx[i][j][k-1]
								- hz[i][j][k] + hz[i-1][j][k]);



							ez[i][j][k] = gi3[i] * gj3[j] * gj3[k] * ez[i][j][k]
								+ gi2[i] * gj2[j] * gj2[k] * DT*(hy[i][j][k] - hy[i - 1][j][k]
								- hx[i][j][k] + hx[i][j - 1][k]);
						}
					}
				}

				/* Sinusoidal Source */
				pulse = sin(2 * pi * 4500 * 1e6*dt*T);
				ez[ic][ic][ic] = pulse;
				for (int x = 35; x < 65; x++)
				{
					for (int z = 45; z < 55; z++)
					{
						if ((z<49 || z>51) || (x<49 || x>51))
						{
							ez[x][20][z] = 0;
							//ez[x][80][z] = 0;

							ex[x][20][z] = 0;
							//ex[x][80][z] = 0;

							ey[x][20][z] = 0;
							//ey[x][80][z] = 0;
						}
						ez[x][80][z] = 0;
						ex[x][80][z] = 0;
						ey[x][80][z] = 0;
					}
				}
				for (int y = 20; y < 80; y++)
				{
					for (int z = 45; z < 55; z++)
					{
						ez[35][y][z] = 0;
						ez[65][y][z] = 0;

						ex[35][y][z] = 0;
						ex[65][y][z] = 0;

						ey[35][y][z] = 0;
						ey[65][y][z] = 0;
					}
				}
				for (int x = 35; x < 65; x++)
				{
					for (int y = 20; y < 80; y++)
					{
						ez[x][y][45] = 0;
						ez[x][y][55] = 0;

						ex[x][y][45] = 0;
						ex[x][y][55] = 0;

						ey[x][y][45] = 0;
						ey[x][y][55] = 0;
					}
				}

				/*for (int k = 0; k < IE; k++)
				{
					for (int g = 0; g < IE; g++)
					{
						if (k<25 || k>35)
						{
							ez[65][k][g] = 0;
						}
						ez[35][k][g] = 0;
					}
				}
				for (int k = 40; k < 58; k++)
				{
					for (int g = 0; g < IE; g++)
					{
						ez[k][15][g] = 0;
						ez[k][IE - 15][g] = 0;
					}
				}*/

				/*pic.memDC->SelectObject(&red_pen);

				pic.memDC->MoveTo(30, (IE-IE/2-15));
				pic.memDC->LineTo(30, (2 * IE + IE / 2 + 25));

				pic.memDC->MoveTo(30, (IE - IE / 2 - 15));
				pic.memDC->LineTo(2.4*98, (IE - IE / 2 - 15));

				pic.memDC->MoveTo(30, (2 * IE + IE / 2 + 25));
				pic.memDC->LineTo(2.4 * 98, (2 * IE + IE / 2 + 25));

				pic.memDC->MoveTo(2.4 * 98, (IE - IE / 2 - 15));
				pic.memDC->LineTo(2.4 * 98, (IE - IE / 2 + 40));

				pic.memDC->MoveTo(2.4 * 98, (IE - IE / 2 + 80));
				pic.memDC->LineTo(2.4 * 98, (2 * IE + IE / 2 + 25));*/
				/* Calculate the Ez field */
				/* Leave the Ez edges to 0, as part of the PML */

				/* Calculate the Hx field */
				for (i = 0; i < IE - 1; i++)
				{
					for (j = 0; j < IE-1; j++)
					{
						for (int k = 0; k < IE-1; k++)
						{
							/*hx[i][j][k] = fj3[i] * hx[i][j][k] + fj2[j] * DT*(ez[i][j][k] - ez[i][j+1][k]);

							hy[i][j][k] = fi3[i] * hy[i][j][k] + fi2[i] * DT*(ez[i + 1][j][k] + ez[i][j][k]);

							hz[i][j][k] = fi3[i] * hz[i][j][k] + fi2[i] * DT* (ez[i][j][k] - ez[i][j][k + 1]);// -*/
								



							/*hx[i][j][k] = hx[i][j][k]
								- DT*(ez[i][j + 1][k] - ez[i][j][k]
								- ey[i][j][k + 1] + ey[i][j][k]);

							hy[i][j][k] = hy[i][j][k]
								- DT*(ex[i][j][k + 1] - ez[i][j][k]
								- ez[i + 1][j][k] + ez[i][j][k]);

							hz[i][j][k] = hz[i][j][k]
								- DT*(ey[i + 1][j][k] - ey[i][j][k]
								- ex[i][j + 1][k] + ez[i][j][k]);*/

							
							hx[i][j][k] = fj3[j] * hx[i][j][k]
								- fj2[j] * DT*(ez[i][j+1][k] - ez[i][j][k]
								- ey[i][j][k+1]+ey[i][j][k]);

							hy[i][j][k] = fi3[i] * hy[i][j][k]
								- fi2[i] * DT*(ex[i][j][k + 1] - ex[i][j][k]
								- ez[i + 1][j][k] + ez[i][j][k]);

							hz[i][j][k] = fi3[i] * hz[i][j][k]
								- fi2[i] * DT*(ey[i+1][j][k] - ey[i][j][k]
								- ex[i][j+1][k] + ex[i][j][k]);
						}
						
					}
				}

				double minVal = -1;
				double maxVal = 1;
				double one;
				double two;
				double one2;
				double two2;
				double kof1, kof2;

				/* Write the E field out to picture */
				for (j = 0; j < JE; j++)
				{
					for (i = 0; i < IE; i++)
					{
						//for (int k = 0; k < IE; k++)
						//{
							int kof = 18;
							int in = 1;
							kof1 = (double)pic.rect1.Width() / (double)IE;
							kof2 = (double)pic.rect1.Height() / (double)IE;

							if (ez[i][j][IE / 2] == 0)
							{
								one = 0;
								two = 0;
							}
							else
							{
								if (ez[i][j][IE / 2] < 0)
								{
									one = 1;
									two = 0;
								}
								else
								{
									one = -1;
									two = -1;
								}
							}
							SETPOINT(kof1*i, kof2*j);
					}
				}
				for (j = 0; j < JE; j++)
				{
					for (i = 0; i < IE; i++)
					{
						//for (int k = 0; k < IE; k++)
						//{
						int kof = 18;
						int in = 1;
						kof1 = (double)pic2.rect2.Width() / (double)IE;
						kof2 = (double)pic2.rect2.Height() / (double)IE;

						if (ez[IE / 2][i][j] == 0)
						{
							one2 = 0;
							two2 = 0;
						}
						else
						{
							if (ez[IE / 2][i][j] < 0)
							{
								one2 = 1;
								two2 = 0;
							}
							else
							{
								one2 = -1;
								two2 = -1;
							}
						}
						SETPOINT2(kof1*i, kof2*j);
					}
				}
				for (j = 0; j < JE; j++)
				{
					for (i = 0; i < IE; i++)
					{
						//for (int k = 0; k < IE; k++)
						//{
						int kof = 18;
						int in = 1;
						kof1 = (double)pic3.rect3.Width() / (double)IE;
						kof2 = (double)pic3.rect3.Height() / (double)IE;

						if (ez[i][19][j] == 0)
						{
							one2 = 0;
							two2 = 0;
						}
						else
						{
							if (ez[i][19][j] < 0)
							{
								one2 = 1;
								two2 = 0;
							}
							else
							{
								one2 = -1;
								two2 = -1;
							}
						}
						SETPOINT3(kof1*i, kof2*j);
					}
				}
				pic1.Redraw1();
				pic2.Redraw2();
				pic3.Redraw3();
				//Sleep(200);
			}
		}
	return;
}


void CFDTDDlg::OnTimer(UINT nIDEvent)
{
	CDialog::OnTimer(nIDEvent);
};



DWORD WINAPI CFDTDDlg::CalculationFDTD(PVOID param)
{
	CFDTDDlg* dlg = (CFDTDDlg*)param;

	/*FDTDCalc::*/dlg->CalcAntonFDTD(dlg->pic, dlg->pic2, dlg->pic3);

	return 0;
}



void CFDTDDlg::OnBnClickedFdtd2d()
{
	// TODO: �������� ���� ��� ����������� �����������
	// TODO: �������� ���� ��� ����������� �����������
	//FDTDCalc::F = true;
	//FDTDCalc::P = false;
	if (hCalcTh_FDTD && WaitForSingleObject(hCalcTh_FDTD, 0) != WAIT_OBJECT_0)
	{
		MessageBox(L"���������� ��� �����������", L"FDTD", MB_OK | MB_ICONSTOP);
		return;
	}
	//�������� �������
	hCalcTh_FDTD = CreateThread(NULL, NULL, CalculationFDTD, (PVOID)this, NULL, NULL);
	//int time = SetTimer(1, 1, NULL);
}

