// *.h file
#include "stdafx.h"
#include "FDTDDlg.h"
#include "Calc.h"
#include <math.h>


void fourea(/*struct */complex *number, int n, int is)
{
	int i, j, istep;
	int m, mmax;
	double r, r1, theta, w_r, w_i, temp_r, temp_i;
	double pi = 3.1415926;

	r = pi*is;
	j = 0;
	for (i = 0; i<n; i++)
	{
		if (i<j)
		{
			temp_r = number[j].real;
			temp_i = number[j].image;
			number[j].real = number[i].real;
			number[j].image = number[i].image;
			number[i].real = temp_r;
			number[i].image = temp_i;
		}
		m = n >> 1;
		while (j >= m) { j -= m; m = (m + 1) / 2; }
		j += m;
	}
	mmax = 1;
	while (mmax<n)
	{
		istep = mmax << 1;
		r1 = r / (double)mmax;
		for (m = 0; m<mmax; m++)
		{
			theta = r1*m;
			w_r = (double)cos((double)theta);
			w_i = (double)sin((double)theta);
			for (i = m; i<n; i += istep)
			{
				j = i + mmax;
				temp_r = w_r*number[j].real - w_i*number[j].image;
				temp_i = w_r*number[j].image + w_i*number[j].real;
				number[j].real = number[i].real - temp_r;
				number[j].image = number[i].image - temp_i;
				number[i].real += temp_r;
				number[i].image += temp_i;
			}
		}
		mmax = istep;
	}
	if (is>0)
	for (i = 0; i<n; i++)
	{
		number[i].real /= (double)n;
		number[i].image /= (double)n;
	}
}
