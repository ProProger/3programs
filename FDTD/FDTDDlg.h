
// FDTDDlg.h : ���� ���������
//
#include "Control.h"
#include "Calc.h"
#include "Triangle.h"

#pragma once
using namespace std;


#define IE 100
#define JE 100

// ���������� ���� CFDTDDlg
class CFDTDDlg : public CDialogEx
{
// ��������
public:
	CFDTDDlg(CWnd* pParent = NULL);	// ����������� �����������

// ������ ����������� ����
	enum { IDD = IDD_FDTD_DIALOG };

	CWnd* frameWnd1; //������� ��� ��������� ��������
	CDC* frameDc1;
	CRect r1;

	CWnd* frameWnd2; //������� ��� ��������� ��������
	CDC* frameDc2;
	CRect r2;

	CWnd* frameWnd3; //������� ��� ��������� ��������
	CDC* frameDc3;
	CRect r3;

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV

// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	double xp1, yp1, xp2, yp2;
	CPen setka_pen; // ��� �����
	CPen osi; // �������� ����� ��� ��������� ����
	CPen graphic; // ��� �������
	CPen red_pen;
	CPen white;
	CBrush e1;
	CBrush e2;
	CBrush e3;
	CBrush e4;
	CBrush e5;

	//������ ���������� ���������
	CPicControl1 pic;
	CPicControl2 pic2;
	CPicControl3 pic3;
	//��������� �������
	static DWORD WINAPI CalculationFDTD(PVOID param);
	//����������� ������
	HANDLE hCalcTh_FDTD;

	bool kn_1;

	
	double ca[IE], cb[IE];



	
	double FDTD_2D();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	int nTimerID;
	double t;
	double pulse;//��������
	double lyambda;
	
	double T;
	int iter;
	double eaf, sigma, epsz, epsilon;
	double member1, member2, member3, memh1, memh2, memh3;
	double mh1, mh2, mh3, me1, me2, me3;
	
	
	void Plot(CWnd* frameWnd, int picID, CDC* frameDc, CRect r, CDC *MDc, CBitmap &bmp, CBitmap *tmp, CPen *ruchka,
		double xmax, double xmin, double xpi, double ypi, double y_1[IE], int n);

	double epsilon0;
	double Mu0;
	//double dt;
	double dz;
	double dy;
	double w;

	//void Calc1D(CPicControl & pic);
	//void Calc2D(CPicControl & pic);
	void CalcAntonFDTD(CPicControl1 & pic1, CPicControl2 & pic2, CPicControl3 & pic3);
	//void CalcAntonPSTD(CPicControl & pic);
	//void myCalc(CPicControl & pic);

	CPen pen, *oldpen;
	CBrush brush, *oldbrush;

	vector <Triangle> AllTriangles;
	afx_msg void OnBnClickedFdtd2d();
	afx_msg void OnBnClickedPstd2d2();




	//�� ������� CalcAntonPSTD
	/*double gi2[IE], gi3[IE];
	double gj2[JE], gj3[IE];
	double fi1[IE], fi2[IE], fi3[JE];
	double fj1[JE], fj2[JE], fj3[JE];

	double xn, xxn, xnum, xd, curl_e, npml;
	double ez[IE][IE];
	double hx[IE][IE];
	double hy[IE][IE];

	double EZ[IE];
	double HX[IE];
	double HY[IE];*/

	double c;
	double dx;
	double dt;
	double freq_in;

	double smena1[IE];

	complex n_ez[IE];
	complex n_hx[IE];
	complex n_hy[IE];


	double ez_pr[IE][IE];
	double hx_pr[IE][IE];
	double hy_pr[IE][IE];

	int N;
	//double ***ez, ***hx, ***hy;
};
