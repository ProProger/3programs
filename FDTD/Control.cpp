#include "stdafx.h"
#include "Control.h"
//////////////////////////////////////////////////////////////////////////���������� ������ ��� ������ � PictureControl

//����������� �� ���������
CPicControl1::CPicControl1()
{
}

CPicControl2::CPicControl2()
{
}

CPicControl3::CPicControl3()
{
}

//����������� � �����������
CPicControl1::CPicControl1(
	CWnd* hDlg1,				//���� � ��������� ����������
	int picID1
	)
{
	//��������� ������������� ���������� �������� � ���������
	wnd1 = hDlg1->GetDlgItem(picID1);
	dc1 = wnd1->GetDC();
	//��������� ���������� ��� ��� ������
	dc1->SetBkMode(TRANSPARENT);
	//������� ������� ��������
	wnd1->GetClientRect(&rect1);

	
	//� Pic �� ����� �������� ������� �����������, ������������ � ������ ����������
	//�����������������	������� ��� ��������� � ������ ����������
	memDC1 = new CDC;
	memDC1->CreateCompatibleDC(dc1);
	//������� CreateCompatibleDC ������� �������� ���������� � ������  (DC), ����������� � �������� �����������.
	bmp1.CreateCompatibleBitmap(dc1, rect1.Width(), rect1.Height());
	tmp1 = memDC1->SelectObject(&bmp1);
	dc1->BitBlt(0, 0, rect1.Width(), rect1.Height(), memDC1, 0, 0, SRCCOPY);
}


CPicControl2::CPicControl2(
	CWnd* hDlg2,				//���� � ��������� ����������
	int picID2
	)
{
	//��������� ������������� ���������� �������� � ���������
	wnd2 = hDlg2->GetDlgItem(picID2);
	dc2 = wnd2->GetDC();
	//��������� ���������� ��� ��� ������
	dc2->SetBkMode(TRANSPARENT);
	//������� ������� ��������
	wnd2->GetClientRect(&rect2);


	//� Pic �� ����� �������� ������� �����������, ������������ � ������ ����������
	//�����������������	������� ��� ��������� � ������ ����������
	memDC2 = new CDC;
	memDC2->CreateCompatibleDC(dc2);
	//������� CreateCompatibleDC ������� �������� ���������� � ������  (DC), ����������� � �������� �����������.
	bmp2.CreateCompatibleBitmap(dc2, rect2.Width(), rect2.Height());
	tmp2 = memDC2->SelectObject(&bmp2);
	dc2->BitBlt(0, 0, rect2.Width(), rect2.Height(), memDC2, 0, 0, SRCCOPY);
}


CPicControl3::CPicControl3(
	CWnd* hDlg3,				//���� � ��������� ����������
	int picID3
	)
{
	//��������� ������������� ���������� �������� � ���������
	wnd3 = hDlg3->GetDlgItem(picID3);
	dc3 = wnd3->GetDC();
	//��������� ���������� ��� ��� ������
	dc3->SetBkMode(TRANSPARENT);
	//������� ������� ��������
	wnd3->GetClientRect(&rect3);


	//� Pic �� ����� �������� ������� �����������, ������������ � ������ ����������
	//�����������������	������� ��� ��������� � ������ ����������
	memDC3 = new CDC;
	memDC3->CreateCompatibleDC(dc3);
	//������� CreateCompatibleDC ������� �������� ���������� � ������  (DC), ����������� � �������� �����������.
	bmp3.CreateCompatibleBitmap(dc3, rect3.Width(), rect3.Height());
	tmp3 = memDC3->SelectObject(&bmp3);
	dc3->BitBlt(0, 0, rect3.Width(), rect3.Height(), memDC3, 0, 0, SRCCOPY);
}



void CPicControl1::Create1(
	CWnd* hDlg1,				//���� � ��������� ����������
	int picID1				//ID �������� ���������
	)
{
	wnd1 = hDlg1->GetDlgItem(picID1);
	dc1 = wnd1->GetDC();
	//��������� ���������� ��� ��� ������
	dc1->SetBkMode(TRANSPARENT);
	//������� ������� ��������
	wnd1->GetClientRect(&rect1);

	memDC1 = new CDC;
	memDC1->CreateCompatibleDC(dc1);
	//������� CreateCompatibleDC ������� �������� ���������� � ������  (DC), ����������� � �������� �����������.
	bmp1.CreateCompatibleBitmap(dc1, rect1.Width(), rect1.Height());
	tmp1 = memDC1->SelectObject(&bmp1);
	dc1->BitBlt(0, 0, rect1.Width(), rect1.Height(), memDC1, 0, 0, SRCCOPY);
}

void CPicControl2::Create2(
	CWnd* hDlg2,				//���� � ��������� ����������
	int picID2				//ID �������� ���������
	)
{
	wnd2 = hDlg2->GetDlgItem(picID2);
	dc2 = wnd2->GetDC();
	//��������� ���������� ��� ��� ������
	dc2->SetBkMode(TRANSPARENT);
	//������� ������� ��������
	wnd2->GetClientRect(&rect2);

	memDC2 = new CDC;
	memDC2->CreateCompatibleDC(dc2);
	//������� CreateCompatibleDC ������� �������� ���������� � ������  (DC), ����������� � �������� �����������.
	bmp2.CreateCompatibleBitmap(dc2, rect2.Width(), rect2.Height());
	tmp2 = memDC2->SelectObject(&bmp2);
	dc2->BitBlt(0, 0, rect2.Width(), rect2.Height(), memDC2, 0, 0, SRCCOPY);
}


void CPicControl3::Create3(
	CWnd* hDlg3,				//���� � ��������� ����������
	int picID3				//ID �������� ���������
	)
{
	wnd3 = hDlg3->GetDlgItem(picID3);
	dc3 = wnd3->GetDC();
	//��������� ���������� ��� ��� ������
	dc3->SetBkMode(TRANSPARENT);
	//������� ������� ��������
	wnd3->GetClientRect(&rect3);

	memDC3 = new CDC;
	memDC3->CreateCompatibleDC(dc3);
	//������� CreateCompatibleDC ������� �������� ���������� � ������  (DC), ����������� � �������� �����������.
	bmp3.CreateCompatibleBitmap(dc3, rect3.Width(), rect3.Height());
	tmp3 = memDC3->SelectObject(&bmp3);
	dc3->BitBlt(0, 0, rect3.Width(), rect3.Height(), memDC3, 0, 0, SRCCOPY);
}


//����������
CPicControl1::~CPicControl1()
{
}

CPicControl2::~CPicControl2()
{
}

CPicControl3::~CPicControl3()
{
}

//����� ����������� �������� �� ������ �� �����
BOOL CPicControl1::Redraw1()
{
	return dc1->BitBlt(0, 0, rect1.Width(), rect1.Height(), memDC1, 0, 0, SRCCOPY);
}

BOOL CPicControl2::Redraw2()
{
	return dc2->BitBlt(0, 0, rect2.Width(), rect2.Height(), memDC2, 0, 0, SRCCOPY);
}

BOOL CPicControl3::Redraw3()
{
	return dc3->BitBlt(0, 0, rect3.Width(), rect3.Height(), memDC3, 0, 0, SRCCOPY);
}

//����� ������� ��������
void CPicControl1::Clear1(COLORREF color /*= RGB(0,0,0)*/)
{
	memDC1->FillSolidRect(rect1, color);
}

void CPicControl2::Clear2(COLORREF color /*= RGB(0,0,0)*/)
{
	memDC2->FillSolidRect(rect2, color);
}

void CPicControl3::Clear3(COLORREF color /*= RGB(0,0,0)*/)
{
	memDC3->FillSolidRect(rect3, color);
}